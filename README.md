<!-- vale off -->
# `devcontainer` `Rye`
<!-- vale on -->

[![Software License](https://img.shields.io/badge/license-MIT-informational.svg?style=for-the-badge)](LICENSE)
[![Pipeline Status](https://img.shields.io/gitlab/pipeline-status/op_so/docker/devcontainer-rye?style=for-the-badge)](https://gitlab.com/op_so/docker/devcontainer-rye/pipelines)

A [`devcontainer`](https://containers.dev/) [Debian image](https://github.com/devcontainers/images/tree/main/src/base-debian) with [`Rye`](https://rye.astral.sh/) to develop in Python inside a container:

* `multiarch` with support of **amd64** and **arm64**,
* **automatically** updated by comparing software bill of materials (`SBOM`) changes,
* image **signed** with [Cosign](https://github.com/sigstore/cosign),
* a **software bill of materials (`SBOM`) attestation** added using [`Syft`](https://github.com/anchore/syft),
* available on **Docker Hub** and **Quay.io**.

[![GitLab](https://shields.io/badge/Gitlab-554488?logo=gitlab&style=for-the-badge)](https://gitlab.com/op_so/docker/devcontainer-rye) The main repository.

[![Docker Hub](https://shields.io/badge/dockerhub-1D63ED?logo=docker&logoColor=white&style=for-the-badge)](https://hub.docker.com/r/jfxs/devcontainer-rye) The Docker Hub registry.

[![Quay.io](https://shields.io/badge/quay.io-E5141F?logo=docker&logoColor=white&style=for-the-badge)](https://quay.io/repository/ifxs/devcontainer-rye) The Quay.io registry.

This image offers an optimal experience for `zsh` shell users. The image also includes `bash` shell.

<!-- vale off -->
## Running with VScode
<!-- vale on -->

`.devcontainer/devcontainer.json` configuration  to [develop inside a container](https://code.visualstudio.com/docs/devcontainers/containers):

```json
{
  "image": "jfxs/devcontainer-rye",
...
}
```

<!-- vale off -->
## Running locally
<!-- vale on -->

```shell
docker run -it --rm -v $(pwd):/workdir jfxs/devcontainer-rye /bin/zsh
```

## Built with

Docker latest tag is [--VERSION--](https://gitlab.com/op_so/docker/devcontainer-rye/-/blob/main/Dockerfile) and has:

<!-- vale off -->
--SBOM-TABLE--
<!-- vale on -->

[`Dockerhub` Overview page](https://hub.docker.com/r/jfxs/devcontainer-rye) has the details of the last published image.

## Versioning

Docker tag definition:

* the rye version used,
* a dash
* an increment to differentiate build with the same version starting at 001

```text
<rye_version>-<increment>
```

<!-- vale off -->
Example: 0.26.0-001
<!-- vale on -->

## Signature and attestation

[Cosign](https://github.com/sigstore/cosign) public key:

```shell
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEa3yV6+yd/l4zh/tfT6Tx+zn0dhy3
BhFqSad1norLeKSCN2MILv4fZ9GA6ODOlJOw+7vzUvzZVr9IXnxEdjoWJw==
-----END PUBLIC KEY-----
```

The public key is also available online: <https://gitlab.com/op_so/docker/cosign-public-key/-/raw/main/cosign.pub>.

To verify an image:

```shell
cosign verify --key cosign.pub $IMAGE_URI
```

To verify and get the software bill of materials (`SBOM`) attestation:

```shell
cosign verify-attestation --key cosign.pub --type spdxjson $IMAGE_URI | jq '.payload | @base64d | fromjson | .predicate'
```

## Authors

<!-- vale off -->
* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)
<!-- vale on -->

## License

<!-- vale off -->
This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
<!-- vale on -->
