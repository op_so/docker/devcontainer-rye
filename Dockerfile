# hadolint ignore=DL3007
FROM mcr.microsoft.com/devcontainers/base:debian

ARG USER=vscode
ARG HOME="/home/$USER"
ARG VERSION
ARG BUILD_DATE
ARG VCS_REF="DEV"

ENV container=docker
ENV HOME="/home/$USER"
ENV PYTHONUNBUFFERED=1
ENV PIP_DISABLE_PIP_VERSION_CHECK=1
ENV RYE_NO_AUTO_INSTALL=1
ENV PATH="$HOME/.rye/shims:$HOME/.local/bin:$PATH"
ENV ZSH_CUSTOM="$HOME/.oh-my-zsh/custom"

LABEL maintainer="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.title="devcontainer-rye" \
    org.opencontainers.image.description="A devcontainer image with rye for Python developpement" \
    org.opencontainers.image.authors="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.version="${VERSION}" \
    org.opencontainers.image.url="https://hub.docker.com/r/jfxs/devcontainer-rye" \
    org.opencontainers.image.source="https://gitlab.com/op_so/docker/devcontainer-rye" \
    org.opencontainers.image.revision=${VCS_REF} \
    org.opencontainers.image.created=${BUILD_DATE}

# hadolint ignore=DL4006,SC2016
RUN mkdir -p $HOME \
    && curl -sSf https://rye.astral.sh/get | RYE_INSTALL_OPTION="--yes" bash \
    && echo 'source "$HOME/.rye/env"' >> ~/.profile \
    && mkdir $ZSH_CUSTOM/plugins/rye \
    && rye self completion -s zsh > $ZSH_CUSTOM/plugins/rye/_rye \
    && sh -c "$(curl --location https://taskfile.dev/install.sh)" -- -d -b $HOME/.local/bin \
    && mkdir $ZSH_CUSTOM/plugins/task \
    && curl -s -o $ZSH_CUSTOM/plugins/task/_task https://raw.githubusercontent.com/go-task/task/main/completion/zsh/_task \
    && chown -R $USER:$USER "$HOME/.rye/" "$HOME/.local/" \
    && rm -rf "$HOME/.cache/" \
    && sed -i 's/^plugins=.*/plugins=(git rye task)/' "$HOME/.zshrc"

CMD ["rye", "--version"]
